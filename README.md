## Synopsis

NWT Swedish Language Pack

## Motivation

No motivation

## Contributors

https://www.nordicwebteam.se

## Installation

### Manual 
 * download and copy all files  into app/i18n/nwt/language-sv_se directory


### Git (Submodule)
  * if you use git into your magento folder, use submodule (or modman)
  
 `git submodule add git@bitbucket.org:nordicwebteam/nwt-sv-language-m2.git app/i18n/nwt/language-sv_se`
 
  * or, if u don't use git into magento folder, use 
  
  `git clone git@bitbucket.org:nordicwebteam/nwt-sv-language-m2.git app/i18n/nwt/language-sv_se`

### Modman
  `modman clone git@bitbucket.org:nordicwebteam/nwt-sv-language-m2.git` (the module will be deployed into **app/i18n/nwt/language-sv_se**)

### Composer

* the module will be deployed into `vendor/nwt/language-sv_se`

   Add 

     {
        "type": "vcs",
        "url": "git@bitbucket.org:nordicwebteam/nwt-sv-language-m2.git"
     }

   into **composer.json, repositories section**
    
   Run 

     composer require --prefer-source  'nwt/language-sv_se:*'

   After that (eventually) flush the cache 

     bin/magento cache:flush 
